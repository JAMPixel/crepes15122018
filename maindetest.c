#include <SANDAL2/SANDAL2.h>
#include <stdio.h>
#include <stdlib.h>
#include "perso.h"
#include "enigme.h"
#include "niveau.h"
#include "defined.h"


void event_manager(int (*statement)(float))
{
  int   tps, ticks, oldTps = 0;
  float dt;
  int   run = 1;
  int * dataw;

  while(run)
    {
      tps = SDL_GetTicks();
      dt = tps - oldTps;
      oldTps = tps;

      run = !PollEvent(NULL);

      getDataWindow((void **)&dataw);

      run = run && dataw;
      dataw = (run)?dataw:0;

      if(statement)
	run = run && statement(dt);

      updateWindow();
      displayWindow();
      ticks = 16 - SDL_GetTicks() + tps;

      if(ticks > 0)
	SDL_Delay(ticks);

      if(initIteratorWindow())
	run = 0;

    }
}

int main()
{
  int black[] = {0,0,0,0};
  //int green[]={0,255,0,0};
  int dataw = 1;
  //Element * character;
  //Element * b1,* b2;
  //Element * porteDeTest;

  if(initAllSANDAL2(IMG_INIT_JPG))
    {
      puts("Failed to init SANDAL2");
      return EXIT_FAILURE;
    }

  createWindow(WIDTH, HEIGHT, NAME, 0, black, GAME);

  if(initIteratorWindow())
    {
      closeAllSANDAL2();
      fprintf(stderr, "Failed to open window \n");
      return EXIT_FAILURE;
    }

  setDataWindow(&dataw);

  /*porteDeTest=createBlock(500,100,100,200,green,0,0);

  character=createChar(500,500,50,100,NULL);
  b1 = createActivable(character,porteDeTest,500,500,20,20,sprite,bouton);
  b2 = createActivable(character,NULL,400,500,20,20,sprite,entreeCode);*/
  creationNiveau(1);

  event_manager(NULL);

  return EXIT_SUCCESS;
}
