EXEC=exec
SRC_DIR=src/
BUILD_DIR=build/
SRC=$(notdir $(wildcard $(SRC_DIR)*.c))
OBJ=$(addprefix $(BUILD_DIR), $(SRC:.c=.o))
H=$(wildcard $(SRC_DIR)*.h)
C=gcc
CFLAGS=-g -Wall -Wextra
LIBS=-lSANDAL2 -lm -lSDL2 -lSDL2_image -lSDL2_ttf -lpthread -lSDL2_mixer


all:testBuild $(EXEC)
testBuild:
	@if [ -d $(BUILD_DIR) ];	then	echo;else mkdir $(BUILD_DIR);	fi;
$(EXEC):$(OBJ)
	@echo "\e[1;32mEdition des liens.\e[0;37m"
	@$(C) $^ -o $@ $(LIBS)
	@echo "\e[1;35mCompilation terminée.\e[0;37m"
$(BUILD_DIR)%.o:$(SRC_DIR)%.c $(H)
	@echo "\e[0;36mCompilation de $<. \e[0;37m"
	@$(C) -o $@ -c $< $(CFLAGS)
clean:
	rm -rf *~* *#* $(SRC_DIR)*~* $(SRC_DIR)*#*
clear:clean
	rm -rf $(BUILD_DIR)* $(EXEC)
