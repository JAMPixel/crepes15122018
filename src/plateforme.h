#ifndef PLATEFORME_H
#define PLATEFORME_H

#include <SANDAL2/SANDAL2.h>

typedef struct
{
  int x;
  int y;
  int w;
  int h;
}Platform;

Element * createPlateform(const char * path, int x, int y, int w, int h);

#endif /* PLATEFORME_H */
