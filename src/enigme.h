#ifndef __ENIGME__
#define __ENIGME__

#include <SANDAL2/SANDAL2.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "perso.h"
#include "defined.h"
#include "datawindow.h"
#include "niveau.h"

//def de la struct des activables

typedef struct{
  void (*effect)(Element *);
  char codeSortie[25];
  Element * cible;
  float x;
  float y;
  int tps;
  int fram;
}actif_t;

//création des élément activables
//args : perso, cible, coord de la cible, coord de l'activable, taille de l'activable, nom du spritesheet, fct à appeller
Element * createActivable(Element * character,Element * elemCible,float xc,float yc,float x,float y, float w, float h,char sprite[],void (*effetActif)(Element *));

//effet des activables
void activable(Element * this,SDL_Keycode c);
void bouton_apparition(Element *);
void entreeCode(Element *);
void saisirCode(Element * this,SDL_Keycode c);
void aff_enigme(Element *);
void bouton_piege(Element * this);
void bouton_timer(Element * this);
void plat_timer(Element *);
#endif
