#include <SANDAL2/SANDAL2.h>
#include <SDL2/SDL_mixer.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

#include "perso.h"
#include "defined.h"
#include "plateforme.h"
#include "moteur.h"
#include "datawindow.h"
#include "niveau.h"
#include "waterwater.h"

void move_goutte2 (Element * this);

int usb_open(char const * filename) {

	struct termios options;
	int fd = open(filename, O_RDWR|O_NOCTTY);
	if(fd < 0) return fd;

	fcntl(fd, F_SETFL, 0);
	tcgetattr(fd, &options);
	usleep(10000);
	cfsetospeed(&options, B9600);
	cfsetispeed(&options, B9600);
	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;
	options.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL|IXON);
	options.c_oflag &= ~CRTSCTS;
	options.c_oflag &= ~OPOST;
	options.c_lflag &= ~(ICANON|ECHO|ECHONL|IEXTEN|ISIG);

	options.c_cc[VMIN] = 1;
	options.c_cc[VTIME] = 40;

	tcsetattr(fd, TCSANOW, &options);
	tcflush(fd, TCIOFLUSH);
	usleep(10000);

	return fd;
}
void menu(void);
void click(Element*this,int i);

void event_manager()
{
  int   tps, ticks, oldTps = 0;
  float dt;
  int   run = 1;
  DataWindow * dataw;

  getDataWindow((void **)&dataw);

  while(run)
    {
      tps = SDL_GetTicks();
      dt = tps - oldTps;
      oldTps = tps;

      run = !PollEvent(NULL);

      run = run && dataw->param == 1;

      if(dataw->param != 2)
	       dataw->param = (run)?dataw->param:0;

      run = run && run_mgr(dt);

      updateWindow();
      displayWindow();
      ticks = 16 - SDL_GetTicks() + tps;

      if(ticks > 0)
	      SDL_Delay(ticks);

      if(initIteratorWindow())
	      run = 0;
    }

    //printf("%d\n", dataw->param);
}

int main()
{
  int black[4] = {0,0,0,0};
  DataWindow * dataw = initDataWindow();
  void (*level[])() = {level1,level2,level3,level4,level5};
  int lvl = 0;
  int size_lvl = sizeof(level) / sizeof(void (*)());
  FILE * save = fopen("save.lvl", "r");

  Mix_Music * music;

  if(initAllSANDAL2(IMG_INIT_JPG|IMG_INIT_PNG))
    {
      puts("Failed to init SANDAL2");
      return EXIT_FAILURE;
    }

  if(SDL_Init(SDL_INIT_AUDIO) < 0)
    {
      puts("AHAHA FAIL !");
    }

  if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT,2,2048) < 0)
    {
      puts("SDL_Mixer error");
    }

  createWindow(WIDTH, HEIGHT, NAME, 0, black, MENU);

  setDataWindow(dataw);

  if(initIteratorWindow())
    {
      closeAllSANDAL2();
      fprintf(stderr, "Failed to open window \n");
      return EXIT_FAILURE;
    }
  menu();

  fscanf(save, "%d", &lvl);
  fclose(save);

  if(lvl < 0 || lvl >= size_lvl) lvl = 0;


  music = Mix_LoadMUS("sounds/music.wav");

  if(!music)
    printf("NON\n");
  else
    Mix_PlayMusic(music, -1);

  while(dataw->param && lvl < size_lvl) {
    createBaseLevel();
    dataw->param = 1;
    level[lvl]();
    event_manager();
    clearDisplayCode(0);
    lvl += dataw->param == 2;
  }

  setDisplayCodeWindow(2);
  updateWindow();
  displayWindow();
  SDL_Delay(1000);

  save = fopen("save.lvl", "w");
  fprintf(save, "%d\n", lvl);

  fclose(save);

  Mix_Quit();

  closeAllWindow(); /* close all windows */
  closeAllSANDAL2();

  return EXIT_SUCCESS;
}

typedef struct{
  int go;
} demarrer_t;

void menu(){
	Element * goutte;
	DataW   * datab = malloc(sizeof(DataW));
  int green[]={0,255,0,0};
  demarrer_t * lancer=(demarrer_t *)malloc(sizeof(demarrer_t));
  Element * btnStart;
  lancer->go=0;
  createText(WIDTH/2-350,HEIGHT/2-100,700,200,100,"pipe.ttf","Merci",green,SANDAL2_BLENDED,2,0);
  createText(WIDTH/2-350,HEIGHT/2-100,700,200,100,"pipe.ttf","The Fuite",green,SANDAL2_BLENDED,1,0);
  btnStart=createText(WIDTH/2-150,HEIGHT/2+100,300,100,100,"pipe.ttf","START",green,SANDAL2_BLENDED,1,0);
  setDataElement(btnStart,(void *)lancer);
  addClickableElement(btnStart,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(btnStart,click);
	initDataWater(datab);
	goutte = createImage(WIDTH/2-350+700-134*lgout/225,HEIGHT/2-100+200-lgout,134*lgout/225,lgout,"sprite/goutte2.png",1,-4);
	goutte->data = datab;
  setActionElement(goutte,move_goutte2);


  while(!lancer->go){
    PollEvent(NULL);
		updateWindow();
	  displayWindow();
		SDL_Delay(16);
  }
  setDisplayCodeWindow(0);
  clearDisplayCode(1);
}

void click(Element * this,int i){
  (void)i;
  demarrer_t * lancer;
  getDataElement(this,(void **)&lancer);
  lancer->go=1;
}

void move_goutte2 (Element * this){
	DataW * datab;
  DataWindow * dataw;

  datab = this->data;

  getDataWindow((void **)&dataw);

  acceleration(this, &datab->dt, dataw->gravity);
	if (this->y > HEIGHT+lgout+800){
		this->y=HEIGHT/2-100+200-lgout;
	  datab->dt = 0;
	}

  this->data = datab;
}
