#include "enigme.h"


Element * createActivable(Element * character,Element * elemCible,float xc,float yc,float x,float y, float w, float h,char sprite[],void (*effetActif)(Element *)){
  //int red[]={255,0,0,0};
  Element * actif;
  actif_t * structactif=(actif_t *)malloc(sizeof(actif_t));

  //creation du bouton
  //actif =createBlock(x,y,w,h,red,0,1);
  //printf("%s\n",sprite);
  actif = createImage(x,y,w,h,sprite,0,1);

  //ajout des animations
  addAnimationElement(actif,0);
  addSpriteAnimationElement(actif,0,0,0,20,40,5,0);
  addSpriteAnimationElement(actif,0,20,0,20,40,5,1);
  setWaySpriteAnimationElement(actif,0,0);

  //init et ajout de la data
  structactif->effect=effetActif;
  structactif->cible=elemCible;
  structactif->x=xc;
  structactif->y=yc;
  setDataElement(actif,(void *)structactif);


  //chainage et creation de l'effet
  addElementToElement(actif,character);
  setKeyPressedElement(actif,activable);

  return(actif);
}

void activable(Element * this,SDL_Keycode c){
  float xp,yp,wp,hp,xb,yb,wb,hb;
  Element * character=NULL;
  actif_t * structactif=NULL;

  //récup du perso
  initIteratorElement(this);
  character=nextIteratorElement(this);

  //récup de la data
  getDataElement(this,(void **)&structactif);

  //récup des coordonnées
  getCoordElement(character,&xp,&yp);
  getCoordElement(this,&xb,&yb);
  //recup des dims
  getDimensionElement(character,&wp,&hp);
  getDimensionElement(this,&wb,&hb);

  //effect
  if(c==13 && xp+wp>xb && xp<xb+wb && yp+hp>yb && yp<yb+hb){
    //delElement(this);
    structactif->effect(this);
  }
}

void bouton_apparition(Element * this){

  actif_t * structactif;
  Element * temp;

  DataWindow * dataw;
  getDataWindow((void **)&dataw);

  //recup de la cible
  getDataElement(this,(void **)&structactif);

  //changer le sprite du bouton (bouton appuyé)
  setSpriteAnimationElement(this,1);

  //effet
  //addDisplayCodeElement(structactif->cible,0,3);
  temp=createImage(structactif->x,structactif->y,100,20,PLATFORM,0,3);
  addElementToElement(dataw->plaform,temp);
  //printf("DING\n");
}

void entreeCode(Element * this){
  int red[]={255,0,0,0};
  int white[]={255,255,255,0};
  Element * entree;
  Element * character;

  //recup du perso
  initIteratorElement(this);
  character=nextIteratorElement(this);
  //moveElement(character,100,0);

  //creation de l'entry
  entree=createEntry(100,100,100,70,100,"arial.ttf","Cliquer ici",red,SANDAL2_BLENDED,white,0,0,0,10,0);

  //bloque les mvts
  setKeyPressedElement(character,NULL);
  addElementToElement(entree,character);
  moveElement(character,W_PERSO,0);

  //ajout des events de entry
  addClickableElement(entree,rectangleClickable(0.f,0.f,1.f,1.f),0);
  if(setKeyPressedElement(entree,saisirCode))
    printf("error\n");
  //animation
  setSpriteAnimationElement(this,1);
}

void saisirCode(Element * this,SDL_Keycode c){
  //printf("ASCII de %c: %d\n",c,c);
  Element * character;
  perso_t * structchar=NULL;
  char * code=(char *)malloc(10*sizeof(char));

  //recup du perso
  initIteratorElement(this);
  character=nextIteratorElement(this);

  //recup de la data du perso
  getDataElement(character,(void **)&structchar);

  if(c==8)
    delCharEntry(this);
  else
    if(c==13){
      getTextElement(this,&code);
      structchar->code_saisie=code;
      //printf("%s\n",structchar->code_saisie);
      setFreeDataElement(this,NULL);
      setKeyPressedElement(character,mouvementsPress);
      delElement(this);
      verification(character,structchar->code_saisie,structchar->code_ladder);
    }else
      if((c>=97 && c<=122) || (c>=48 && c<=57))
        addCharEntry(this,(char)c);
}

void aff_enigme(Element * this){
  actif_t * structactif;
  Element * ecran;
  level_t * indice;

  //recup de la cible
  getDataElement(this,(void **)&structactif);
  ecran=structactif->cible;
  getDataElement(ecran,(void **)&indice);

  setImageElement(ecran,indice->image_code);
  setPlanElement(ecran,0,1);
  setSpriteAnimationElement(this,1);
  //printf("%s\n",indice->image_code);
  /*createImage(785,HEIGHT-2.5*H_PERSO+5,190,160,indice->image_code,0,3)*/;
}

void bouton_piege(Element * this){
  actif_t * structactif;
  Element * elemCible;
  float x,y;
  srand(time(0));

  setSpriteAnimationElement(this,1);

  //recup de la cible
  getDataElement(this,(void **)&structactif);
  elemCible=structactif->cible;
  getCoordElement(elemCible,&x,&y);

  replaceElement(elemCible,rand()%WIDTH,y);
}

void bouton_timer(Element * this){

    actif_t * structactif,* timed=(actif_t *)malloc(sizeof(actif_t));
    Element * but_timed;

    DataWindow * dataw;
    getDataWindow((void **)&dataw);

    //recup de la cible
    getDataElement(this,(void **)&structactif);

    //changer le sprite du bouton (bouton appuyé)
    setSpriteAnimationElement(this,1);

    //effet
    //addDisplayCodeElement(structactif->cible,0,3);
    but_timed=createImage(structactif->x,structactif->y,100,20,PLATFORM,0,3);
    addElementToElement(dataw->plaform,but_timed);
    setDataElement(but_timed,(void *)timed);
    timed->fram=0;
    timed->tps=10;
    setActionElement(but_timed,plat_timer);
}

void plat_timer(Element * this){
  actif_t *timed;
  DataWindow * dataw;
  getDataWindow((void **)&dataw);
  getDataElement(this,(void **)&timed);
  if(timed->tps>0){
    if(timed->fram==15){
      timed->fram=0;
      timed->tps--;
    }
    timed->fram++;
  }else{
    delElementToElement(dataw->plaform,this);
    delElement(this);
  }
}
