#include <stdlib.h>

#include "plateforme.h"

Element * createPlateform(const char * path, int x, int y, int w, int h)
{
  Element  * platform = createImage(x, y, w, h, path, 0, 0);
  Platform * data = malloc(sizeof(Platform)); 
  int        green[] = {0,255,0,0};
  
  
  if(platform == NULL) {
    platform = createBlock(x, y, w, h, green, 0, 0);
  }
  
  data->x = x;
  data->y = y;
  data->w = w;
  data->h = h;
  
  setDataElement(platform, data);
  setFreeDataElement(platform, free);

  return platform;
}
