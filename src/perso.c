#include "perso.h"
#include "datawindow.h"

Element * createChar(float x, float y){
  perso_t * structchar=(perso_t *)malloc(sizeof(perso_t));

  //création du perso
  Element * character = createImage(x,y,50,101,"sprite/sprite_sheet.png",0,0);

  //init et ajout structure
  structchar->r=0;
  structchar->l=0;
  structchar->jump=0;
  structchar->h_jump=0;
  structchar->frames=0;
  structchar->platform = NULL;
  structchar->code_saisie=(char *)malloc(10*sizeof(char));
  structchar->code_ladder=(char *)malloc(10*sizeof(char));
  structchar->plouf = Mix_LoadWAV("sounds/plouf.wav");
  structchar->breath = 10000;

  //puis on rajoutera les animations
  addAnimationElement(character,0); // normal droite
  addSpriteAnimationElement(character,0,50,101,50,101,10,0);
  setWaySpriteAnimationElement(character,0,1);

  addAnimationElement(character,1); // course droite
  addSpriteAnimationElement(character,1,100,101,50,101,30,0);
  addSpriteAnimationElement(character,1,50,101,50,101,10,0);
  addSpriteAnimationElement(character,1,0,101,50,101,30,0);
  addSpriteAnimationElement(character,1,50,101,50,101,10,0);
  setWaySpriteAnimationElement(character,1,1);

  addAnimationElement(character,2); // normal gauche
  addSpriteAnimationElement(character,2,50,0,50,101,10,0);
  setWaySpriteAnimationElement(character,2,1);

  addAnimationElement(character,3); // course gauche
  addSpriteAnimationElement(character,3,0,0,50,101,30,0);
  addSpriteAnimationElement(character,3,50,0,50,101,10,0);
  addSpriteAnimationElement(character,3,100,0,50,101,30,0);
  addSpriteAnimationElement(character,3,50,0,50,101,10,0);
  setWaySpriteAnimationElement(character,3,1);

  setAnimationElement(character,0);

  //mouvements du perso
  setKeyPressedElement(character,mouvementsPress);
  setKeyReleasedElement(character,mouvementsUnpress);
  setActionElement(character,deplacement);

  setDataElement(character,(void *)structchar);
  return(character);
}

void mouvementsPress(Element * this,SDL_Keycode c){
  //printf("ASCII de %c: %d\n",c,c);
  perso_t * structchar=NULL;
  //recup de la data
  getDataElement(this,(void **)&structchar);


  if(!structchar->collide) {
    switch (c) {
      //LEFT
    case SDLK_LEFT:{
      structchar->l=1;
      structchar->r=0;
      setAnimationElement(this,3);
      break;
    }
      //RIGHT
    case SDLK_RIGHT:{
      structchar->r=1;
      structchar->l=0;
      setAnimationElement(this,1);
      break;
    }
    case SDLK_SPACE:{
      if(structchar->jump!=1){
        structchar->jump=1;
        structchar->h_jump=5;
      }
      break;
    }
    }
  }

}

void mouvementsUnpress(Element * this,SDL_Keycode c){
  perso_t * structchar=NULL;
  //recup de la data
  getDataElement(this,(void **)&structchar);

  if(!structchar->collide) {

    switch (c) {
      //LEFT
    case SDLK_LEFT:{
      structchar->l=0;
      setAnimationElement(this,2);
      break;
    }
      //RIGHT
    case SDLK_RIGHT:{
      structchar->r=0;
      setAnimationElement(this,0);
      break;
    }
    case SDLK_SPACE:{
      structchar->jump=0;
      break;
    }
    }
  }
}

void deplacement(Element * this){
  perso_t * structchar=NULL;
  float xp,yp;
  int xw, yw;
  DataWindow * dataw;
  int tmp;

  getDimensionWindow(&xw, &yw);

  getDataWindow((void **)&dataw);
  getCoordElement(this,&xp,&yp);
  getDataElement(this,(void **)&structchar);

  tmp = structchar->submerged;
  
  structchar->submerged = collisionElement(this, dataw->water) || collisionElement(this, dataw->water_up);

  if(!tmp && structchar->submerged) Mix_PlayChannel(1, structchar->plouf, 0);
  
  if(!structchar->collide) {

    moveElement(this,(10 >> structchar->submerged)*(structchar->r-structchar->l)/2,-structchar->h_jump);

    if(structchar->h_jump>0 && structchar->frames%10==0){
      structchar->frames=0;
      structchar->h_jump--;
    }
    structchar->frames++;
  }

  if(yp<0 || yp > yw){
    dataw->param = 2 + (yp > yw);
  }

  if(xp < 0) {
    replaceElement(this, 6, yp);
  }

  if(xp + this->width > xw) {
    replaceElement(this, xw - this->width - 6, yp);
  }
}

void monter(Element * this,SDL_Keycode c){
  Element * character;
  float xp,yp,xl,yl,wp,hp,wl,hl;

  //recup du perso
  initIteratorElement(this);
  character=nextIteratorElement(this);

  getCoordElement(character,&xp,&yp);
  getCoordElement(this,&xl,&yl);
  getDimensionElement(character,&wp,&hp);
  getDimensionElement(this,&wl,&hl);

  if(c==SDLK_UP && xp+wp>=xl && xp<=xl+wl && yp+hp>=yl && yp<=yl+hl){
    moveElement(character,0,-25);
  }

}
