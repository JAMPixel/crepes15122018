#ifndef __WATERWATER_H__
#define __WATERWATER_H__

#include <SANDAL2/SANDAL2.h>
#include <SDL2/SDL_mixer.h>

typedef struct DataWater{
  int nb_goutte;
  int r;
  int fin;
  float dt;
  Mix_Chunk * sound;
}DataW;

void initDataWater(DataW * datab);
void move_goutte (Element * this);
void affiche_tuyaux(void);

#endif
