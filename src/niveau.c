#include <pthread.h>

#include "niveau.h"

int usb_open(char const * filename);

void createBaseLevel()
{
  Element * character;
  Element *ground;
  DataWindow * dataw;
  //Element * ecran;
  //level_t * indice=(level_t *)malloc(sizeof(level_t));
  //char spritesheet[]="sprites/levier.png";

  getDataWindow((void **)&dataw);

  ground = createPlateform(PLATFORM, 0, HEIGHT, WIDTH, 10);
  addElementToElement(ground, ground);

  //affichage des tuyaux
  affiche_tuyaux();

  createImage(0,0,WIDTH,HEIGHT,"sprites/mur.png",0,10);

  //creation de l'ecran
  character=createChar(100,HEIGHT-H_PERSO-100);
  createImage(390,10,200,170,"sprites/ecran.png",0,3);
  /*ecran=createImage(395,15,190,160,"sprites/fond.png",0,4);
  strcpy(indice->image_code,"sprites/cheval.png");
  setDataElement(ecran,(void *)indice);
  createActivable(character,ecran,0,0,500,HEIGHT-2.5*H_PERSO-40,20,40,spritesheet,aff_enigme);*/

  setDataWindow(dataw);
  addElementToElement(ground, ground);


  addPersoToWindow(character);
  dataw->plaform = ground;

  affiche_tuyaux();

  createImage(0, 0, WIDTH, HEIGHT, "sprite/mur.png", 0, 10);
}

void level1(){
  level_t * indice=(level_t *)malloc(sizeof(level_t));
  Element * character;
  Element * pt[20];
  Element *ground;
  DataWindow * dataw;
  Element * ecran;
  perso_t * structchar;
  char codeniv[] = "cheval";
  char spritesheet[]="sprites/levier.png";
  //level_t * pmouv=(level_t *)malloc(sizeof(level_t));

  getDataWindow((void **)&dataw);

  character = dataw->perso;
  ground = dataw->plaform;

  getDataElement(character,(void **)&structchar);
  //printf(" %s %s \n", structchar->code_ladder,codeniv);
  strcpy(structchar->code_ladder,codeniv);
  //printf(" %s %s \n", structchar->code_ladder,codeniv);

  ecran=createImage(395,15,190,160,"sprites/fond.png",0,4);
  strcpy(indice->image_code,"sprites/cheval.png");
  setDataElement(ecran,(void *)indice);
  createActivable(character,ecran,0,0,500,HEIGHT-2.5*H_PERSO-40,20,40,spritesheet,aff_enigme);

  //creation affichage des plateformes
  pt[0]=createImage(0,0,0,0,PLATFORM,0,0);
  //createImage(300,HEIGHT-H_PERSO,100,20,PLATFORM,1,3);
  //setDisplayCodeElement(pt[0],0,0); //fait disparaitre le bloc (graphiquement ?)

  pt[1]=createImage(450,HEIGHT-2*H_PERSO,100,20,PLATFORM,0,0);
  pt[2]=createImage(550,HEIGHT-2.2*H_PERSO,100,20,PLATFORM,0,0);
  pt[3]=createImage(650,HEIGHT-3*H_PERSO,100,20,PLATFORM,0,0);
  pt[4]=createImage(750,HEIGHT-4*H_PERSO,100,20,PLATFORM,0,0);
  pt[5]=createImage(850,HEIGHT-4.2*H_PERSO,100,20,PLATFORM,0,0);
  pt[6]=createImage(950,HEIGHT-4*H_PERSO,100,20,PLATFORM,0,0);

  //pt[7]=createImage(0,0,0,0,PLATFORM,0,0);
  //createImage(550,HEIGHT-3.7*H_PERSO,100,20,PLATFORM,1,3);
  //setDisplayCodeElement(pt[7],0,0);

  //pt[8]=createImage(0,0,0,0,PLATFORM,0,0);
  //createImage(350,HEIGHT-4.3*H_PERSO,100,20,PLATFORM,1,3);
  //setDisplayCodeElement(pt[8],0,0);

  //pt[7]=createImage(250,HEIGHT-3.5*H_PERSO,100,20,PLATFORM,0,0);
  pt[7]=createImage(150,HEIGHT-3.5*H_PERSO-20,100,20,PLATFORM,0,0);
  pt[8]=createImage(50,HEIGHT-3.5*H_PERSO-40,100,20,PLATFORM,0,0);

  //plein de tests

  //pt[9]=createImage(200,HEIGHT-50,100,20,PLATFORM,0,0);
  //createActivable(character,NULL,200,600,40,HEIGHT-H_PERSO-20,20,40,spritesheet,bouton_timer);
  /*pmouv->sens=-1;
  setDataElement(pt[9],(void *)pmouv);
  setActionElement(pt[9],plat_mouv);*/

  //chainage
  for(int k=0;k<9;k++){
    addElementToElement(ground,pt[k]);
  }
  //printf("%s\n",spritesheet);
  //creation des mecanismes
  createActivable(character,pt[0],300,HEIGHT-H_PERSO,20,HEIGHT-H_PERSO/2-20,20,40,spritesheet,bouton_apparition);
  createActivable(character,pt[7],550,HEIGHT-3.7*H_PERSO,1000,HEIGHT-4.5*H_PERSO-20,20,40,spritesheet,bouton_apparition);
  createActivable(character,pt[8],350,HEIGHT-4.3*H_PERSO,1000,HEIGHT-H_PERSO/2-20,20,40,spritesheet,bouton_apparition);
  createActivable(character,NULL,0,0,70,HEIGHT-4*H_PERSO-40,20,40,spritesheet,entreeCode);


}

void verification(Element * character,char code_utilisateur[],char code_niveau[]){
  Element * ladder;
  if(!strcmp(code_utilisateur,code_niveau)){
    ladder=createImage(170,0,70,HEIGHT-3.5*H_PERSO-20,"sprites/ladder.png",0,3);
    addElementToElement(ladder,character);
    setKeyPressedElement(ladder,monter);
  }
}

void * lvl3(void * args)
{
  FILE * file = (FILE *)args;
  DataWindow * dataw;
  perso_t * p;
  char  str[30];
  char spritesheet[]="sprites/levier.png";

  getDataWindow((void **)&dataw);
  getDataElement(dataw->perso, (void **)&p);

  fgets(str, 30, file);
  //printf("AH\n");
  //p->r = 1;

  createActivable(dataw->perso,NULL,0,0,70,HEIGHT-2*H_PERSO-40,20,40,spritesheet,entreeCode);
  addElementToElement(dataw->plaform, createImage(0,HEIGHT-2*H_PERSO + 50,100,20,PLATFORM,0,0));
  addElementToElement(dataw->plaform, createImage(150,HEIGHT-4*H_PERSO + 50,100,20,PLATFORM,0,0));

  return NULL;
}




void plat_mouv(Element * this){
  level_t * pmouv;
  getDataElement(this,(void **)&pmouv);
  float x,y;
  getCoordElement(this,&x,&y);
  if(y<300)
    pmouv->sens=1;
  if(y>HEIGHT)
    pmouv->sens=-1;

  //printf("%d\n",pmouv->sens);
  moveElement(this,0,pmouv->sens);
}

void level2(){
  Element * ecran;
  level_t * indice=(level_t *)malloc(sizeof(level_t));
  Element * character;
  Element * pt[20];
  Element *ground;
  DataWindow * dataw;
  perso_t * structchar;
  char codeniv[] = "crepe";
  char spritesheet[]="sprites/levier.png";
  //level_t * pmouv=(level_t *)malloc(sizeof(level_t));

  getDataWindow((void **)&dataw);

  character = dataw->perso;
  ground = dataw->plaform;

  getDataElement(character,(void **)&structchar);
  strcpy(structchar->code_ladder,codeniv);

  ecran=createImage(395,15,190,160,"sprites/fond.png",0,4);
  strcpy(indice->image_code,"sprites/rebus.png");
  setDataElement(ecran,(void *)indice);
  createActivable(character,ecran,0,0,500,HEIGHT-2.5*H_PERSO-40,20,40,spritesheet,aff_enigme);

  //creation des plateformes
  pt[0]=createImage(450,HEIGHT-2*H_PERSO,100,20,PLATFORM,0,0);
  pt[1]=createImage(710,HEIGHT-2*H_PERSO,100,20,PLATFORM,0,0);
  pt[2]=createImage(920,HEIGHT-3*H_PERSO,100,20,PLATFORM,0,0);
  pt[3]=createImage(450,HEIGHT-4*H_PERSO,100,20,PLATFORM,0,0);

  pt[4]=createImage(150,HEIGHT-3.5*H_PERSO-20,100,20,PLATFORM,0,0);
  pt[5]=createImage(50,HEIGHT-3.5*H_PERSO-40,100,20,PLATFORM,0,0);
  //chainage
  for(int k=0;k<6 ;k++){
    addElementToElement(ground,pt[k]);
  }

  //creation des mecanismes
  createActivable(character,character,0,0,200,HEIGHT-H_PERSO/2-10,20,40,spritesheet,bouton_piege);
  createActivable(character,character,0,0,250,HEIGHT-H_PERSO/2-10,20,40,spritesheet,bouton_piege);
  createActivable(character,NULL,600,HEIGHT-H_PERSO,300,HEIGHT-H_PERSO/2-10,20,40,spritesheet,bouton_apparition);
  createActivable(character,character,0,0,350,HEIGHT-H_PERSO/2-10,20,40,spritesheet,bouton_piege);
  createActivable(character,NULL,780,HEIGHT-4*H_PERSO,970,HEIGHT-3.5*H_PERSO-10,20,40,spritesheet,bouton_timer);
  createActivable(character,NULL,0,0,70,HEIGHT-4*H_PERSO-40,20,40,spritesheet,entreeCode);

}

void level4(){
  Element * ecran;
  level_t * indice=(level_t *)malloc(sizeof(level_t));
  Element * character;
  Element * pt[20];
  Element *ground;
  DataWindow * dataw;
  perso_t * structchar;

  getDataWindow((void **)&dataw);


  char codeniv[] = "canard";
  char spritesheet[]="sprites/levier.png";
  //level_t * pmouv=(level_t *)malloc(sizeof(level_t));

  getDataWindow((void **)&dataw);

  character = dataw->perso;
  ground = dataw->plaform;

  getDataElement(character,(void **)&structchar);
  strcpy(structchar->code_ladder,codeniv);

  ecran=createImage(395,15,190,160,"sprites/fond.png",0,4);
  strcpy(indice->image_code,"sprites/canard.png");
  setDataElement(ecran,(void *)indice);
  createActivable(character,ecran,0,0,500,HEIGHT-2.5*H_PERSO-40,20,40,spritesheet,aff_enigme);

  //creation des plateformes
  pt[0]=createImage(450,HEIGHT-2*H_PERSO,100,20,PLATFORM,0,0);
  pt[1]=createImage(150,HEIGHT-3.5*H_PERSO-20,100,20,PLATFORM,0,0);
  pt[2]=createImage(50,HEIGHT-3.5*H_PERSO-40,100,20,PLATFORM,0,0);
  pt[3]=createImage(10,HEIGHT-H_PERSO-10,100,20,PLATFORM,0,0);
  pt[4]=createImage(550,HEIGHT-2.1*H_PERSO,100,20,PLATFORM,0,0);
  pt[5]=createImage(550,HEIGHT-3.5*H_PERSO,100,20,PLATFORM,0,0);
  //chainage
  for(int k=0;k<6 ;k++){
    addElementToElement(ground,pt[k]);
  }

  //creation des mecanismes
  createActivable(character,NULL,0,0,70,HEIGHT-4*H_PERSO-40,20,40,spritesheet,entreeCode);
  createActivable(character,NULL,120,HEIGHT-2*H_PERSO,60,HEIGHT-2*H_PERSO-10,20,40,spritesheet,bouton_timer);
  createActivable(character,NULL,900,HEIGHT-3*H_PERSO,600,HEIGHT-2.5*H_PERSO-10,20,40,spritesheet,bouton_timer);

}

void level3() {
  char codeniv[] = "pixel";
  int fd = usb_open("/dev/ttyACM0");
  FILE * nucleo;
  pthread_t * th = malloc(sizeof(pthread_t));
  DataWindow * dataw;
  perso_t * structchar;
  Element * character;
  Element * ecran;
  level_t * indice=(level_t *)malloc(sizeof(level_t));
  char spritesheet[]="sprites/levier.png";

  getDataWindow((void **)&dataw);

  if(fd > 0) {
    nucleo = fdopen(fd, "w+");

    character = dataw->perso;

    getDataElement(character,(void **)&structchar);
    strcpy(structchar->code_ladder,codeniv);

    ecran=createImage(395,15,190,160,"sprites/fond.png",0,4);
    strcpy(indice->image_code,"sprites/pixel.png");
    setDataElement(ecran,(void *)indice);
    createActivable(character,ecran,0,0,500,HEIGHT-2.5*H_PERSO-40,20,40,spritesheet,aff_enigme);

    fprintf(nucleo, "level3\n");
    fflush(nucleo);

    pthread_create(th, NULL, lvl3, nucleo);
  }
  else {
    dataw->param = 2;
  }
}

void level5(){
  Element * ecran[3];
  level_t * indice[3];
  Element * character;
  Element * pt[20];
  Element *ground;
  DataWindow * dataw;
  perso_t * structchar;

  getDataWindow((void **)&dataw);


  char codeniv[] = "isima";
  char spritesheet[]="sprites/levier.png";
  //level_t * pmouv=(level_t *)malloc(sizeof(level_t));

  getDataWindow((void **)&dataw);

  character = dataw->perso;
  ground = dataw->plaform;

  getDataElement(character,(void **)&structchar);
  strcpy(structchar->code_ladder,codeniv);

  ecran[0]=createImage(395,15,190,160,"sprites/fond.png",0,4);

  indice[0]=(level_t *)malloc(sizeof(level_t));
  strcpy(indice[0]->image_code,"sprites/II.png");
  setDataElement(ecran[0],(void *)indice[0]);

  ecran[1]=createImage(395,15,190,160,"sprites/MS.png",0,4);

  indice[1]=(level_t *)malloc(sizeof(level_t));
  strcpy(indice[1]->image_code,"sprites/MS.png");
  setDataElement(ecran[1],(void *)indice[1]);

  ecran[2]=createImage(395,15,190,160,"sprites/fond.png",0,4);

  indice[2]=(level_t *)malloc(sizeof(level_t));
  strcpy(indice[2]->image_code,"sprites/A.png");
  setDataElement(ecran[2],(void *)indice[2]);


  //creation des plateformes
  pt[0]=createImage(150,HEIGHT-3.5*H_PERSO-20,100,20,PLATFORM,0,0);
  pt[1]=createImage(50,HEIGHT-3.5*H_PERSO-40,100,20,PLATFORM,0,0);
  pt[2]=createImage(100,HEIGHT-H_PERSO,100,20,PLATFORM,0,0);
  pt[3]=createImage(260,HEIGHT-2*H_PERSO+10,100,20,PLATFORM,0,0);
  pt[4]=createImage(370,HEIGHT-3*H_PERSO+10,100,20,PLATFORM,0,0);
  //chainage
  for(int k=0;k<5;k++){
    addElementToElement(ground,pt[k]);
  }

  //creation des mecanismes
  createActivable(character,ecran[0],0,0,500,HEIGHT-H_PERSO-10,20,40,spritesheet,aff_enigme);
  createActivable(character,ecran[1],0,0,600,HEIGHT-H_PERSO-10,20,40,spritesheet,aff_enigme);
  createActivable(character,ecran[2],0,0,700,HEIGHT-H_PERSO-10,20,40,spritesheet,aff_enigme);
  createActivable(character,NULL,0,0,70,HEIGHT-4*H_PERSO-40,20,40,spritesheet,entreeCode);


}
