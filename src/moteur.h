#ifndef MOTEUR_H
#define MOTEUR_H

#include <SANDAL2/SANDAL2.h>

typedef struct
{
  float x;
  float y;
}Speed;

int collisionElement(Element * elt1, Element * elt2);
int run_mgr(float dt);
void acceleration(Element * elt, float * dt, Speed speed);

#endif /* MOTEUR_H */
