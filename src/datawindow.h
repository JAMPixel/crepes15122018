#ifndef DATAWINDOW_H
#define DATAWINDOW_H

#include <SANDAL2/SANDAL2.h>
#include "moteur.h"
#include "plateforme.h"

typedef struct 
{
  Element * perso;
  Element * plaform;
  Element * water_up;
  Element * water;
  int       param;
  Speed     gravity;
}DataWindow;

DataWindow * initDataWindow();
void addPersoToWindow(Element * elt);

#endif /* DATAWINDOW_H */
