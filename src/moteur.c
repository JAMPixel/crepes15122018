#include <math.h>
#include "moteur.h"
#include "datawindow.h"
#include "perso.h"


int collisionElement(Element * elt1, Element * elt2)
{
  float x1, x2, y1, y2;
  float w1, w2, h1, h2;

  getCoordElement(elt1, &x1, &y1);
  getCoordElement(elt2, &x2, &y2);
  getDimensionElement(elt1, &w1, &h1);
  getDimensionElement(elt2, &w2, &h2);

  return ((x1 + w1 > x2) && ((y1 + h1 > y2) && (y1 < h2 + y2))) &&
    ((x2 + w2 > x1) && ((y1 + h1 > y2) && (y1 < h2 + y2)));
}

void acceleration(Element * elt, float * dt, Speed speed)
{

  //moveElement(elt, *dt * speed.x, *dt*speed.y);

  moveElement(elt, speed.x, *dt * speed.y);
  *dt += 0.02;
}

void replaceCollision(Element * elt, Element * obj)
{
  float x1,y1,x2,y2;
  perso_t * perso;

  getDataElement(elt, (void **)&perso);

  if(!perso->l && !perso->r) {
    getCoordElement(elt, &x1, &y1);
    getCoordElement(obj, &x2, &y2);

    perso->l = x1 < x2;
    perso->r = x1 > x2;
  }

  moveElement(elt, 10*(perso->l - perso->r), 0);
}

Element * searchPlateform(Element * elt, Element * platforms)
{
  Element * e;
  perso_t * perso;

  initIteratorElement(platforms);
  getDataElement(elt, (void **)&perso);

  while((e = nextIteratorElement(platforms))) {
    if(e == elt) continue;
    else {
      if(collisionElement(elt, e) && elt->y + elt->height < e->y + e->height){
	perso->dt = 0;
	return e;
      }
    }
  }

  return NULL;
}

int searchCollision(Element * elt)
{
  Element * e;
  int       ret = 0;
  perso_t * perso;

  initIterator(0);
  getDataElement(elt, (void **)&perso);

  while((e = nextElement())) {
    if(e == elt) continue;
    else {
      if(collisionElement(elt, e) && e != perso->platform && e->codes->first->plan == 0) {
	ret  = 1;
	replaceCollision(elt, e);
      }
    }
  }

  if(perso->collide && !ret) {
    perso->r = perso->l = 0;
  }

  perso->collide = ret;

  return ret;
}

int run_mgr(float dt)
{
  DataWindow * dataw;
  Speed speed = {0,5};
  perso_t * perso;
  Element * platform;

  getDataWindow((void **)&dataw);

  getDataElement(dataw->perso, (void **)&perso);

  if(perso->submerged) 
     speed.y = 2;

  
  if(collisionElement(dataw->perso, dataw->water)) {
    perso->breath -= dt;

    if(perso->breath <= 0) dataw->param = 3;
  }
  else {
    perso->breath = 10000;
    }
  
  acceleration(dataw->perso, &perso->dt, speed);

  platform = searchPlateform(dataw->perso, dataw->plaform);

  if(platform) {
    perso->platform = searchPlateform(dataw->perso, dataw->plaform);
  }

  if(perso->dt < 0.1)
    searchCollision(dataw->perso);


  return dataw->param==1;
}
