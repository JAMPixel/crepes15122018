#ifndef __NIVEAU__
#define __NIVEAU__

#include <SANDAL2/SANDAL2.h>
#include <stdio.h>
#include <string.h>
#include "waterwater.h"
#include "defined.h"
#include "perso.h"
#include "enigme.h"
#include "datawindow.h"

//def de la struct des niveaux
typedef struct{
  char image_code[20];
  int sens;
}level_t;

void createBaseLevel();
void level1();
void level2();
void level3();
void level4();
void level5();
void verification(Element * character,char code_utilisateur[],char code_niveau[]);
void plat_mouv(Element *);
#endif
