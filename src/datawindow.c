#include "datawindow.h"

DataWindow * initDataWindow()
{
  DataWindow * dataw = malloc(sizeof(DataWindow));
  /*int          black[] = {0,0,0,0};*/

  dataw->perso = NULL;
  dataw->param = 1;
  dataw->gravity.x = 0;
  dataw->gravity.y = 10;
  
  return dataw;
}

void addPersoToWindow(Element * elt)
{
  DataWindow * dataw;

  getDataWindow((void **)&dataw);

  dataw->perso = elt;
}
