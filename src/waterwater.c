
#include <stdio.h>
#include <stdlib.h>
#include "waterwater.h"
#include "defined.h"
#include "datawindow.h"
#include "moteur.h"

void initDataWater(DataW * datab){
  
  datab->sound = Mix_LoadWAV("sounds/goutte.wav");
  datab->nb_goutte = 0;
  datab->r = 0;
  datab->fin = 1;
  datab->dt = 0;
}

void move_goutte (Element * this){
  DataW * datab;
  Element * next;
  DataWindow * dataw;
  

  datab = this->data;

  if (datab->r == 1){
    initIteratorElement(this);
    next = nextIteratorElement(this);
    next->y-=5;
    next = nextIteratorElement(this);
    next->y-=5;
    datab->nb_goutte=0;
    datab->r=0;
  }

  initIteratorElement(this);
  next = nextIteratorElement(this);
  getDataWindow((void **)&dataw);

  acceleration(this, &datab->dt, dataw->gravity);

  if (next->y+66 < 0) {
    datab->fin=0;
    dataw->param = 3;
  }
  if (this->y > next->y+10 && datab->fin) {
    Mix_PlayChannel(0, datab->sound, 0);
    this->y=65;
    datab->nb_goutte += 1;
    datab->dt = 0;
  }
  if (datab->nb_goutte != 0 && datab->nb_goutte%TIMER == 0 && datab->r == 0) datab->r = 1;

  this->data = datab;
  
}

void affiche_tuyaux(void){
  Element * water;
  Element * water2;
  Element * goutte;
  Element * eau_montante;
  Element * eau;
  DataW   * datab = malloc(sizeof(DataW));
  int       i;

  DataWindow * dataw;

  getDataWindow((void **)&dataw);

  if (!createImage(WIDTH-xtuyauo,0,500,131,"sprite/timer.png",0,-5)) puts("pb tuyaux haut");
  if (!createImage(WIDTH-131,HEIGHT-ytuyaud,131,500,"sprite/timer2.png",0,-5)) puts("pb tuyaux droit");
  if (!createImage(-23,-23,151,149,"sprite/tuyau-coin_fissuré_haut.png",0,-5)) puts("pb tuyaux fissuré haut");
  if (!createImage(-23,-23,151,149,"sprite/tuyau-coin_fissuré_bas.png",0,-3)) puts("pb tuyaux fissuré bas");
  water = createImage(136+WIDTH-xtuyauo+TAYOL/2-triche,85-TAYOL/2+triche,TAYOH,TAYOL,"sprite/watertuyaux.png",0,-1);
  addAnimationElement(water,0);
  for (i=0;i<348;i+=4){
    addSpriteAnimationElement(water,0,i,0,4,196,3,0);
  }
  setWaySpriteAnimationElement(water,0,1);
  water2 = createImage(WIDTH-112,HEIGHT-ytuyaud+135,TAYOH,TAYOL,"sprite/watertuyaux.png",0,-1);
  addAnimationElement(water2,0);
  setAngleElement(water,-90);
  for (i=0;i<348;i+=4){
    addSpriteAnimationElement(water2,0,i,0,4,196,3,0);
  }
  setWaySpriteAnimationElement(water2,0,1);

  eau_montante = createImage(0,HEIGHT,WIDTH,66,"sprite/mer.png",0,-10);
  dataw->water_up = eau_montante;

  addAnimationElement(eau_montante,0);
  for (i=0;i<WIDTH+1;i+=4){
    addSpriteAnimationElement(eau_montante,0,i,0,WIDTH,66,3,0);
  }
  setWaySpriteAnimationElement(eau_montante,0,1);
  eau = createImage(0,HEIGHT+66,WIDTH,1000,"sprite/foond.png",0,-10);
  dataw->water = eau;

  initDataWater(datab);
  goutte = createImage(65,85,134*lgout/225,lgout,"sprite/goutte.png",0,-4);
  addElementToElement(goutte,eau_montante);
  addElementToElement(goutte,eau);
  goutte->data = datab;
  setActionElement(goutte,move_goutte);

}
