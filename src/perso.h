#ifndef __PERSO__
#define __PERSO__

#include <SANDAL2/SANDAL2.h>
#include <stdio.h>
#include <string.h>
#include <SDL2/SDL_mixer.h>

#include "moteur.h"

typedef struct{
  int       r;
  int       l;
  int       r_collide;
  int       l_collide;
  int       jump;
  int       h_jump;
  int       frames;
  int       collide;
  Speed     speed;
  float     dt;
  Element * platform;
  char    * code_saisie;
  char    * code_ladder;
  int       submerged;
  Mix_Chunk * plouf;
  float     breath;
} perso_t;

//creation du perso avec la position, la taille et le fichier de sprite
Element * createChar(float x, float y);

//déplacements
void mouvementsPress(Element * this,SDL_Keycode c);
void mouvementsUnpress(Element * this,SDL_Keycode c);
void deplacement(Element * this);
void monter(Element * this,SDL_Keycode c);

#endif
